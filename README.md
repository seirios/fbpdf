# fbpdf
A script for viewing pdf's from console (framebuffer), under fbterm + byobu/screen

This is a simple bash script for viewing pdf's from console. It can also be used to view PDFs in X without a PDF viewer (e.g. for minimal/remote setups), as long as you have *pdftocairo* and a PNG viewer (e.g. *feh*).

Usage: fbpdf [PDF File]

Controls inside the script loop include page navigation, zooming, trimming of margins.

My setup has fbterm (with image background) running over console and byobu running over fbterm.

+ It makes use of *pdftocairo* for converting single pages to images and *fbv* for viewing them.
+ It also makes use of *file* and *pdfinfo* to extract information about the file.
+ It also makes use of *mogrify* (ImageMagick) for trimming of margins.

I know there are other options available for viewing pdf's on the framebuffer, but this one plays particularly well with fbterm + byobu. It has its quirks, but it works well.

I haven't tested it under a different setup, but I'd expect it to work with minor modifications.
