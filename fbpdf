#!/bin/bash

BGIMAGE="$HOME/bgimage1.jpg" #fbterm background image
RESTORE_BG="fbv -ciuker \"$BGIMAGE\" <<< q; clear" #restore background as in fbterm-bi
VIEW_CMD="fbv --noinfo --noclear" #command to view generated PNGs
DPI=100 #default resolution
TRIM=0 #trim margins flag
FIT=0 #fit to screen height flag
BIAS=0 #page bias
YRES=1050 #monitor y resolution

file=$(basename "$1")

if [ ! -f "$1" ]; then
	echo "[Error] $file : File does not exist or is not a regular file"
	exit
fi

ispdf=$(file "$1" | grep -o PDF)
if [ -z "$ispdf" ]; then

	echo "[Error] $file : File is not a PDF document"
	exit

else

pags=$(pdfinfo "$1" 2>/dev/null | grep Pages: | awk '{print $2}')
if [ -z "$pags" ]; then
	echo "[Error] $file : Cannot determine total number of pages"
	exit
fi

p=1
while [ 1 ]; do
	if [ $FIT -eq 1 ]; then
	pdftocairo -q -png -scale-to-y $YRES -scale-to-x -1 -f $p -l $p -singlefile "$1" "/tmp/{$file}_$$"
	else
	pdftocairo -q -png -r $DPI -f $p -l $p -singlefile "$1" "/tmp/${file}_$$"
	fi
	if [ $TRIM -eq 1 ]; then
		mogrify -trim -bordercolor White -border 20x20 +repage "/tmp/${file}_$$.png"
	fi
	clear ; $VIEW_CMD "/tmp/${file}_$$.png" ; clear
	echo "fbpdf v0.1"
	echo "PDF File: $file"
	echo "[page $p($(expr $p - $BIAS))/$pags($(expr $pags - $BIAS))] [DPI $DPI] [trim $TRIM] [fit $FIT]"
	echo "Enter command..."
	echo "n -- next page"
	echo "b -- previous page"
	echo "g -- goto page"
	echo "B -- page bias"
	echo "r -- reload page"
	echo "- -- decrease resolution"
	echo "+ -- increase resolution"
	echo "d -- set resolution"
	echo "f -- toggle fit to screen height"
	echo "t -- toggle trim margins"
	echo "x -- reset background"
	echo "q -- quit"
	while [ 1 ]; do
	read -n 1 -s cmd
	if [ "$cmd" == "b" ]; then
		p=$(expr $p - 1)
		if [ $p -lt 1 ]; then
			p=1
		fi
		break
	elif [ "$cmd" == "n" ]; then
		p=$(expr $p + 1)
		if [ $p -gt $pags ]; then
			p=$pags
		fi
		break
	elif [ "$cmd" == "B" ]; then
		while [ 1 ]; do
		read -p "Page bias: " BIAS
		if [ ! -z $BIAS ]; then
		if [ $BIAS -ge $(expr -$pags) -a $BIAS -le $pags ]; then
			break
		fi
		fi
		done
		break
	elif [ "$cmd" == "g" ]; then
		while [ 1 ]; do
		read -p "Page: " p
		p=$(expr $p + $BIAS)
		if [ ! -z $p ]; then
		if [ $p -ge 1 -a $p -le $pags ]; then
			break
		fi
		fi
		done
		break
	elif [ "$cmd" == "r" ]; then
		p=$p
		break
	elif [ "$cmd" == "f" ]; then
		if [ $FIT -eq 0 ]; then
			FIT=1
		else
			FIT=0
		fi
		break
	elif [ "$cmd" == "-" ]; then
		DPI=$(expr $DPI - 5)
		if [ $DPI -lt 50 ]; then
			DPI=50
		fi
		break
	elif [ "$cmd" == "+" ]; then
		DPI=$(expr $DPI + 5)
		if [ $DPI -gt 600 ]; then
			DPI=600
		fi
		break
	elif [ "$cmd" == "d" ]; then
		while [ 1 ]; do
		read -p "DPI: " DPI
		if [ ! -z $DPI ]; then
		if [ $DPI -ge 50 -a $DPI -le 600 ]; then
			break
		fi
		fi
		done
		break
	elif [ "$cmd" == "t" ]; then
		if [ $TRIM -eq 0 ]; then
			TRIM=1
		else
			TRIM=0
		fi
		break
	elif [ "$cmd" == "x" ]; then
		eval "$RESTORE_BG"
		break
	elif [ "$cmd" == "q" ]; then
		rm -f "/tmp/${file}_$$.png"
		eval "$RESTORE_BG"
		exit
	fi
	done
done

fi
